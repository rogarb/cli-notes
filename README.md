**WARNING: This program has been deprecated in favor of [rnotes](https://gitlab.com/rogarb/rnotes), a rewrite in Rust**
----

`cli-notes`: a command-line notes manager
------

Installation
----
This program doesn't depend on external libs for compilation. Simply type `make`
to get the `notes` executable that you can copy somewhere in your `$PATH`.

For execution, it needs the `tree` and `rm` utils. The first is from package
`tree` and the second from package `coreutils` in Debian.
Type:
`sudo apt install tree coreutils` (though coreutils should already be present)
It needs also the `$EDITOR` environment variable to be set. See Getting started
below

Getting started
----
Set the `NOTES_DIR` environment variable to an existing directory, i.e.
`export NOTES_SIR=$USER/.notes` or
`echo NOTES_DIR=$USER/.notes >> ~/.bashrc && source ~/.bashrc"`.
The same can be done for `EDITOR`.
`EDITOR` can receive custom options through the `NOTES_ED_OPTS`. This can be
used i.e. with vim to force some specific syntax highlighting without an 
extension: 
`export EDITOR=/usr/bin/vim && export NOTES_ED_OPTS="-c set ft=asciidoc"`

Design
----
The `notes` application manages the notes hierarchy from its root at `$NOTES_DIR`
env variable through subcommands: `notes <command> [note]`
 - `create <note>`: create a new note (and the directory structure if necessary)
 - `edit <note>`: edit an existing note
 - `rm <note|dir>`: remove an existing note or a dir (with everything it contains)
 - `ls [subdir]`: lists the full hierarchy or a subdirectory
 - `append <note>`: redirect stdin to the end of the note (to be used with a pipe)
 - `show <note>`: display the note on screen using `NOTES_VIEWER`
 - `dump <note>`: dump the note on STDOUT
 - `help`: display the usage help

Each note consists of a plaintext file without an extension.

The first line of a file created withes `notes` contains the date of creation.

Edition is carried out through a call to `$EDITOR`, whereas rendering is using
`$NOTES_VIEWER`

Implementation
----
This program is based on `locale.h` in order to natively support utf-8 strings
throught char * type and thus non-ascii characters. Files are accessed throught
the `stdio.h` FILE * type.
