#include <stdio.h>
#include <string.h>
#include <locale.h>

#include "command.h"
#include "configfile.h"

int main(int argc, char *argv[]) 
{
    setlocale(LC_ALL, "");

    setup();

    parse_commandline(argc, argv);

    quit();
    return 0;
}
