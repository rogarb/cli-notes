#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "utils.h"
#include "macro.h"
#include "file.h"
#include "configfile.h"
#include "git.h"

#define MAX_LINE_SIZE	512	    /* max chars per line in the config file */

config_t *conf = NULL;

/* private functions */
static char *get_configfile(void);
static void parse_config(char *configfile);
static void env_config(void);

/* setup the configuration, exits on error */
void setup()
{
    char *configfile = get_configfile();
    char *tmp = NULL;

    if ((conf = malloc(sizeof(config_t))) == NULL)
	error("unable to allocate memory for configuration");
    memset(conf, 0, sizeof(*conf));

    if (file_exists(configfile)) {
	/* parse config */
	parse_config(configfile);
	/* free malloc()'ed string */
    } else {
	warn("no config file found");
    }
    free(configfile);

    /* overwrite the config with the environment variables */
    env_config();

    /* check the config */
    if (conf->notesdir == NULL)
	error("Notes dir path must be set either with environment variable %s "
	      " or %s entry in configfile", NOTESDIR_ENV, CONF_NOTESDIR_KEY);

#ifdef WITH_GIT
    if (conf->git)
	setup_git();
    dbg("Git %s", conf->git ? "enabled" : "disabled");
#else
    __nop_git(); /* to avoid warning when WITH_GIT is not defined */
#endif /* WITH_GIT */
    dbg("using %s as notesdir", conf->notesdir);

}

void quit(void)
{
#ifdef WITH_GIT
    if (conf->git)
	exit_git();
#endif /* WITH_GIT */
    exit(0);
}

/* build up the configfile full path and return it as a malloc()'ed string */
static char *get_configfile(void)
{
    char *cf = NULL;
    char *fmt = "%s/%s/%s"; /* $HOME/CONFIG_DIR/CONFIG_FILE */
    char *home = NULL;
    
    if ((home = getenv(HOME_ENV)) == NULL)
	error("Environment variable %s is not set.", HOME_ENV);

    cf = build_str(fmt, home, CONFIG_DIR, CONFIG_FILE);
    return cf;
}

/* parse the configuration file at configfile */
static void parse_config(char *configfile)
{
    char line_buf[MAX_LINE_SIZE+1] = { 0 };
    FILE *f = fopen(configfile, "r");
    int line_count = 0;
    char *tmp = NULL;

    if (f == NULL)
	error("unable to open file %s readonly: %s", 
		configfile, strerror(errno));

    while (fgets(line_buf, MAX_LINE_SIZE, f) != NULL) {
	if (line_buf[MAX_LINE_SIZE-1] == '\n') /* long line */
	    error("Long line detected in the config file\nAborting\n");

	line_count++;

	if (line_buf[0] == '#' || line_buf[0] == '\n')
	    continue; /* ignore commented lines or empty lines */

	char *comment = strchr(line_buf, '#');
	if (comment != NULL)
	    *comment = '\0'; /* strip comments */

	char *value = line_buf;
	char *key = strsep(&value, "=");

	if (value == NULL)
	    error("invalid content at line %d: %s", line_count, line_buf);

	/* remove starting and ending spaces in strings */
	strip_spaces(key);
	strip_spaces(value);
	strip_quotes(value);
	if (strcmp(key, CONF_NOTESDIR_KEY) == 0) {
	    tmp = strdup(value);
	    conf->notesdir = expand(tmp);
	} else if (strcmp(key, CONF_EDITOROPTS_KEY) == 0) {
	    tmp = strdup(value);
	    conf->ed_opts = tmp;
	} else if (strcmp(key, CONF_VIEWER_KEY) == 0) {
	    tmp = strdup(value);
	    conf->viewer = tmp;
#ifdef WITH_GIT
	} else if (strcmp(key, CONF_GIT_KEY) == 0) {
	    conf->git = atoi(value);
	    if (!is_number(value) || (conf->git != 0 && conf->git != 1))
		error("invalid value %s for key %s at line %d",
		       value, CONF_GIT_KEY, line_count);
#endif /* WITH_GIT */
	} else {
	    error("invalid key %s at line %d in configfile",
		    key, line_count);
	}
    }
}

/* get the configuration from the environment variables */
static void env_config(void)
{
    char *tmp = NULL;

    if ((tmp = getenv(NOTESDIR_ENV)) != NULL)
	conf->notesdir = tmp;
    if ((tmp = getenv(EDITOROPTS_ENV)) != NULL)
	conf->ed_opts = tmp;
    if ((tmp = getenv(VIEWER_ENV)) != NULL)
	conf->viewer = tmp;
}
