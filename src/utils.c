#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <limits.h>
#include <stdarg.h>

#include "configfile.h"
#include "macro.h"
#include "utils.h"

/* returns 1 if string contains only digits (0 to 9), 0 elsewise */
int is_number(char *string)
{
    char *p = string;
    int ret = 1;
    char c;

    while ((c = *p++)) {
	if (!isdigit(c)) {
	    ret = 0;
	    break;
	}
    }
    dbg("\"%s\" is%sa number", string, (ret ? " " : " not "));
    return ret;
}

/* checks the existence of item in list */
int has_item(int item, int *list, int count)
{
    int ret = 0;

    for (int i = 0 ; i < count ; i++) {
	if (list[i] == item) {
	    ret = 1;
	    break;
	}
    }
    return ret;
}

/* returns the total lenght of the strings in the array */
int sum_strlen(int count, char **array)
{
    int len = 0;
    if (array == NULL)
	return len;

    for (int i = 0 ; i < count ; i++)
	    len += strlen(*array++);
    return len;
}

/* returns the number of digits of the argument in base 10 representation
 * it was previously implemented as a macro using log10 but the "bruteforce"
 * version is a lot more efficient and removes the dependency on math lib */
int intlen(int i)
{
    int n = 0;	    /* number of digits */
    /* account for one sign if i < 0 */
    int neg = (i < 0 ? 1 : 0);

    /* take the absolute value */
    if (i < 0)
	i = (i == INT_MIN ? INT_MAX : -i); /* because -INT_MIN overflows */

    if (i < 10)
	n = 1;
    else if (i < 100)
	n = 2;
    else if (i < 1000)
	n = 3;
    else if (i < 10000)
	n = 4;
    else if (i < 100000)
	n = 5;
    else if (i < 1000000)
	n = 6;
    else if (i < 10000000)
	n = 7;
    else if (i < 100000000)
	n = 8;
    else if (i < 1000000000)
	n = 9;
#if INT_MAX == 9223372036854775807  /* int is 64 bits */
    else if (i < 10000000000)
	n = 10;
    else if (i < 100000000000)
	n = 11;
    else if (i < 1000000000000)
	n = 12;
    else if (i < 10000000000000)
	n = 13;
    else if (i < 100000000000000)
	n = 14;
    else if (i < 1000000000000000)
	n = 15;
    else if (i < 10000000000000000)
	n = 16;
    else if (i < 100000000000000000)
	n = 17;
    else if (i < 1000000000000000000)
	n = 18;
    else if (i < 10000000000000000000)
	n = 19;	    /* 2^63 - 1 hits here, for 64 bit ints 
		     * (works also with 32 bit ints */
    else
	n = 20;
#else
    else
	n = 10;
#endif	/* INT_MAX == 9223372036854775807 */


    return n + neg;
}

/* execute a command built with a format string */
void exec_cmd(const char *fmt, ...)
{
    char *cmd = NULL;
    char *p = (char *) fmt; /* fix compiler warning */
    int len = strlen(fmt);
    int s_count = 0;
    int d_count = 0;
    va_list args;

    va_start(args, fmt);

    while (*p) {
	if (*p++ == '%') {
	    switch (*p) {
		case '%':
		    p++;
		    break;
		case 's':
		    len += strlen(va_arg(args, char *));
		    s_count++;
		    break;
		case 'd':
		    len += intlen(va_arg(args, int));
		    d_count++;
		    break;
		default:
		    error("BUG: unexpected identifier %c", *p);
		    break;
	    }
	}
    }

    va_end(args);

    /* allocate memory for cmd */
    if ((cmd = malloc((len+1)*sizeof(char))) == NULL)
	error("unable to allocate %d bytes of memory: %s",
	    len+1, strerror(errno));

    memset(cmd, 0, len+1);
    /* use vsnprintf to fill cmd */
    va_start(args, fmt);
    vsnprintf(cmd, len, fmt, args);
    va_end(args);

    dbg("fmt = \"%s\", got command \"%s\"", fmt, cmd);
    if (system(cmd) == -1)
	error("unable to execute command \"%s\": %s", cmd, strerror(errno));
    free(cmd);
}

/* IN PLACE remove beginning and ending spaces and newlines in string
 * /!\ string has to be writable (not a pointer to const line string = "bla") */
void strip_spaces(char *string)
{
    dbg("received \"%s\"", string);

    char *s;
    char *p;

    /* strip starting spaces */
    while ( *string == ' ') {
	s = string;
	p = string+1;
	while (*p)
	   *s++ = *p++;
    }
    
    s = string + strlen(string) - 1; /* last non NULL char of string */

    /* strip ending spaces and newlines */
    while (*s == ' ' || *s == '\n') {
	*s = '\0';
	if (s-- == string)
	    break;
    }
    dbg("sent \"%s\"", string);
}

/* IN PLACE remove beginning and ending quotes in string
 * /!\ string has to be writable (not a pointer to const line string = "bla") */
void strip_quotes(char *string)
{
    dbg("received \"%s\"", string);

    char *s;
    char *p;

    /* strip starting quote */
    if (*string == '"') {
	s = string;
	p = string+1;
	while (*p)
	   *s++ = *p++;
    }

    s = string + strlen(string) - 1; /* last non NULL char of string */

    /* strip ending quote */
    dbg("*s points to %c", *s);
    while (*s == '"') {
	*s = '\0';
	if (s-- == string)
	   break;
    }
    dbg("sent \"%s\"", string);
}

/* expands a path containing shell globbing characters
 * takes a malloc()'ed string and expands it using realloc()
 * currently suported characters: ~
 */
char *expand(char *path)
{
    if (path == NULL)
	return NULL;
    else if (*path != '~')
	return path;

    int ini_len = strlen(path);
    char *home_env = "HOME";
    char *home = getenv(home_env);
    int len = ini_len-1; /* retrieve 1 as we don't need space for the first 
			  * ~ char */

    if (home == NULL)
	error("environment variable %s is not set", home_env);

    len += strlen(home);
    if ((path = realloc(path, (len+1)*sizeof(char))) == NULL)
	error("unable to reallocate memory");

    /* rework path to insert home at the beginning */
    memmove(path+strlen(home), path+1, ini_len-1);
    memcpy(path, home, strlen(home));
    path[len] = '\0';	/* realloc doesn't zero memory so the last char is 
			 * garbage */
    return path;
}

/* returns a malloc()'ed string containing the basedir of the given path */
char *get_basedir(char *path)
{
    char *last_slash = strrchr(path, '/');
    char *basedir = NULL;
    int len;

    if (last_slash != NULL) {
	len = last_slash - path;
	str_malloc(basedir, len);
	memcpy(basedir, path, len);
    }

    return basedir;
}

/* returns a malloc()'ed string containing the basename of the given path */
char *get_basename(char *path)
{
    char *last_slash = strrchr(path, '/');
    char *basename = NULL;
    int len;

    if (last_slash != NULL) {
	len = strlen(last_slash+1);
	str_malloc(basename, len);
	memcpy(basename, last_slash+1, len);
    }

    return basename;
}

/* return a malloc()'ed string containing the printed string
 * uses format string as printf (for now only %d and %s are supported) */
char *build_str(char *fmt, ...)
{
    char *str = NULL;
    char *p = fmt; /* fix compiler warning */
    int len = strlen(fmt);
    int s_count = 0;
    int d_count = 0;
    va_list args;

    va_start(args, fmt);

    while (*p) {
	if (*p++ == '%') {
	    switch (*p) {
		case '%':
		    p++;
		    break;
		case 's':
		    len += strlen(va_arg(args, char *));
		    s_count++;
		    break;
		case 'd':
		    len += intlen(va_arg(args, int));
		    d_count++;
		    break;
		default:
		    error("BUG: unexpected identifier %c", *p);
		    break;
	    }
	}
    }

    va_end(args);

    /* allocate memory for cmd */
    str_malloc(str, len);

    /* use vsnprintf to fill cmd */
    va_start(args, fmt);
    vsnprintf(str, len, fmt, args);
    va_end(args);

    /* shrink string to actually used memory */
    len = strlen(str);
    if ((str = realloc(str, len+1)) == NULL)
	error("unable to reallocate memory: %s", strerror(errno));
    return str;
}
