#ifdef WITH_GIT
#include <stdio.h>
#include <git2.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>

#include "git.h"
#include "macro.h"
#include "configfile.h"
#include "utils.h"
#include "file.h"

#define GITIGNORE_FILE ".gitignore"
#define GITIGNORE_LEN  (strlen(GITIGNORE_FILE))
#define GITIGNORE_FMT  ".*.swp\n"

#define USER_NAME   "notes manager"
#define USER_EMAIL  "(none)"

/* global variables */
git_repository *repo = NULL;
char *repo_basedir = NULL;

/* private functions */
static int is_git_repo(char *basedir);
static int file_is_in_repo(char *basename);

/* create .gitignore and set it to ignore swapfiles */
void create_gitignore(char *basedir)
{
    char *gitignore = NULL;
    FILE *f = NULL;
    gitignore = build_str("%s/%s", basedir, GITIGNORE_FILE);
    if ((f = fopen(gitignore, "w")) == NULL)
	error("error opening %s in write mode: %s", gitignore, strerror(errno));
    fprintf(f, GITIGNORE_FMT);
    dbg("created .gitignore");
    /* cleanup */
    fclose(f);
    free(gitignore);
}

void exit_git(void)
{
    git_repository_free(repo);
    git_libgit2_shutdown();
}

/* commit one file to repo */
void commit(const char *msg, char *file)
{
    commit_multi(msg, 1, file);
}

/* commit to repo */
/* variadic version accepting multiple args */
void commit_multi(const char *msg, int nfiles, ...)
{
    int status = 0;
    char *file;
    va_list argptr;
    git_index *idx = NULL;
    git_oid tree_id, commit_id;
    git_tree *tree = NULL;
    git_signature *sig = NULL;
    git_reference *ref = NULL;
    git_commit *parent = NULL;
    size_t parent_count = 0;

    /* get repo index */
    exec_git(git_repository_index(&idx, repo), "unable to get repo index");

    /* add args to index */
    va_start(argptr, nfiles);
    for (int i = 0 ; i < nfiles ; i++) {
	file = va_arg(argptr, char *);
	dbg("adding file %s", file);
	if (!file_is_in_repo(file))
	    error("BUG: attempt to access file %s which doesn't exist in %s",
		    file, repo_basedir);
	exec_git(git_index_add_bypath(idx, file),
		"unable to add file %s to index", file);
    }
    /* write modifications to index */
    exec_git(git_index_write(idx), "unable to write index");
    /* create a tree from the index */
    exec_git(git_index_write_tree(&tree_id, idx),
	"Unable to write initial tree from index");
    /* get the tree object from its oid */
    exec_git(git_tree_lookup(&tree, repo, &tree_id), 
	    "unable to get tree from oid");

    /* create a signature */
    exec_git(git_signature_now(&sig, USER_NAME, USER_EMAIL),
	    "unable to create a signature");

    /* check if HEAD exists, if not it is an initial commit and parents is
     * NULL */
    if ((status = git_repository_head(&ref, repo)) == 0) {
	/* HEAD exists, get the oid corresponding to HEAD */
	dbg("found HEAD");
	git_oid tmp_oid;
	exec_git(git_reference_name_to_id(&tmp_oid, repo, "HEAD"),
		"unable to get oid for HEAD");
	exec_git(git_commit_lookup(&parent, repo, &tmp_oid),
		"unable to extract commit at HEAD");
	parent_count = 1;
    } else if (status != GIT_ENOTFOUND) {
	exec_git(status, "error getting HEAD");
    }
    /* if status == GIT_ENOTFOUND, HEAD is missing and we assume that the
     * repo is empty, we leave parent and parent_count zero initialized as they
     * are the correct parameters for git_commit_create_v when repo is empty */

    /* create commit with defined parameters */
    exec_git(git_commit_create_v(&commit_id, repo, "HEAD", sig, sig,
		NULL, msg, tree, parent_count, parent),
	    "unable to create commit \"%s\"", msg);

    /* cleanup */
    if (parent)
	git_commit_free(parent);
    git_reference_free(ref);
    git_tree_free(tree);
    git_signature_free(sig);
    git_index_free(idx);
}

/* commit all files in the repo */
void commit_all(char *msg)
{
    int status = 0;
    git_index *idx = NULL;
    git_oid tree_id, commit_id;
    git_tree *tree = NULL;
    git_signature *sig = NULL;
    git_reference *ref = NULL;
    git_commit *parent = NULL;
    char *path[] = { "*" };
    git_strarray path_array = { path, 1};
    size_t parent_count = 0;

    /* get repo index */
    exec_git(git_repository_index(&idx, repo), "unable to get repo index");

    /* add all files */
    exec_git(git_index_add_all(idx, &path_array, GIT_INDEX_ADD_DEFAULT, NULL, NULL),
	    "unable to add all files to index");
    /* write modifications to index */
    exec_git(git_index_write(idx), "unable to write index");
    /* create a tree from the index */
    exec_git(git_index_write_tree(&tree_id, idx),
	"Unable to write initial tree from index");
    /* get the tree object from its oid */
    exec_git(git_tree_lookup(&tree, repo, &tree_id), 
	    "unable to get tree from oid");

    /* create a signature */
    exec_git(git_signature_now(&sig, USER_NAME, USER_EMAIL),
	    "unable to create a signature");

    /* check if HEAD exists, if not it is an initial commit and parents is
     * NULL */
    if ((status = git_repository_head(&ref, repo)) == 0) {
	/* HEAD exists, get the oid corresponding to HEAD */
	dbg("found HEAD");
	git_oid tmp_oid;
	exec_git(git_reference_name_to_id(&tmp_oid, repo, "HEAD"),
		"unable to get oid for HEAD");
	exec_git(git_commit_lookup(&parent, repo, &tmp_oid),
		"unable to extract commit at HEAD");
	parent_count = 1;
    } else if (status != GIT_ENOTFOUND) {
	exec_git(status, "error getting HEAD");
    }
    /* if status == GIT_ENOTFOUND, HEAD is missing and we assume that the
     * repo is empty, we leave parent and parent_count zero initialized as they
     * are the correct parameters for git_commit_create_v when repo is empty */

    /* create commit with defined parameters */
    exec_git(git_commit_create_v(&commit_id, repo, "HEAD", sig, sig,
		NULL, msg, tree, parent_count, parent),
	    "unable to create commit \"%s\"", msg);

    /* cleanup */
    if (parent)
	git_commit_free(parent);
    git_reference_free(ref);
    git_tree_free(tree);
    git_signature_free(sig);
    git_index_free(idx);
}

void setup_git(void)
{
    char *basedir = conf->notesdir;
    
    if (basedir == NULL)
	error("BUG: basedir is not set");
    repo_basedir = basedir;

    git_libgit2_init();

    /* create the repo if it doesn't exist */
    if (!is_git_repo(basedir)) {
	exec_git(git_repository_init(&repo, basedir, 0),
		"unable to init git repo at %s", basedir);
	dbg("created repo at %s", basedir);
	create_gitignore(repo_basedir);
	commit_all("Initial commit");
    }
    else {
	/* open the repository */
	exec_git(git_repository_open(&repo, basedir),
		 "unable to open repo at %s", basedir);
	dbg("opened repo at %s", basedir);
    }
}

/* checks if we can open a repository at basedir (libgit2 way of checking the
 * existence of a reop */
static int is_git_repo(char *basedir)
{
    int ret = 0;
    if (file_exists(basedir))
	if (git_repository_open_ext(NULL, basedir, 
		    GIT_REPOSITORY_OPEN_NO_SEARCH, NULL) == 0)
	    ret = 1;
    return ret;
}

/* returns 1 if file is in repo dir */
static int file_is_in_repo(char *file)
{
    if (repo_basedir == NULL)
	error("BUG: repo_basedir is not set");

    char *fullpath = build_str("%s/%s", repo_basedir, file);
    int ret = file_exists(fullpath);
    free(fullpath);
    return ret;
}
#else
void __nop_git(void) /* to avoid warning when WITH_GIT is not defined */
{
}
#endif /* WITH_GIT */
