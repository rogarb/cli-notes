#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#include "file.h"
#include "macro.h"
#include "configfile.h"

/* returns a malloc()'ed string containing the full path to filename */
char *build_full_path(char *filename)
{
    char *fullpath =
	malloc((strlen(filename) + strlen(conf->notesdir) + 2)*sizeof(char));
    /* +2 for extra '/' and '\0' */
    strcpy(fullpath, conf->notesdir);
    strcat(fullpath, "/");
    strcat(fullpath, filename);

    if (fullpath == NULL)
	error("Unable to allocate memory: %s\n", strerror(errno));

    return fullpath;
}

/* check that the file exists, we expect a full path !!
 * return 1 if file exists, 0 elsewise */
int file_exists(char *filename)
{
    struct stat unused;

    if (stat(filename, &unused) < 0)
	return 0;

    return 1;
}

/* checks if the given path is a directory, we expect a full path
 * return 1 if path is a directory, 0 elsewise */
int path_is_dir(char *path)
{
    struct stat s;

    /* return 0 if there is an error accessing the stat structure */
    if (stat(path, &s) < 0)
	return 0;

    if (S_ISDIR(s.st_mode)) /*  we use the POSIX macro to test*/
	return 1;
    return 0;
}

/* return the mode of a file given a file descriptor */
int get_mode(int fd)
{
    struct stat s;

    if (fstat(fd, &s) < 0)
	error("Unable to get fd stats: %s\n", strerror(errno));

    return (s.st_mode & 07777);
}

/* recursive function  function to create all the necessary subdirs */
void create_dir(int dirfd, char *file)
{
    char *first_pos = strchr(file, '/');
    if (first_pos == NULL)
	return;	/* no dir to create */
    size_t len = first_pos - file;
#ifdef DEBUG
    printf("Found '/' at pos %p (begining at %p), len = %d\n", first_pos, file, len);
#endif
    char *dirname = malloc((len+1)*sizeof(char));
    memset(dirname, 0, len+1);
    strncpy(dirname, file, len);
#ifdef DEBUG
    printf("Set dirname to %s (full filename: %s)\n", dirname, file);
#endif
    mkdirat(dirfd, dirname, get_mode(dirfd));
    int fd = openat(dirfd, dirname,  O_DIRECTORY);
    if (fd < 0)
	error("Unable to open fd for %s: %s\n", dirname, strerror(errno));
    free(dirname);
    create_dir(fd, first_pos+1);
    close(fd);
}

void create_file(char *filename)
{
    time_t now;
    int fd = open(conf->notesdir, O_DIRECTORY);
    create_dir(fd, filename);
    close(fd);

    FILE *file = fopen(build_full_path(filename), "w");
    if (file == NULL)
	error("Unable to create %s: %s\n", filename, strerror(errno));

    now = time(NULL);
    /* print timestamp */
    fprintf(file, "Created %s", ctime(&now)); /* format string doesn't end with
					       * '\n' as it is in the string
					       * returned by ctime */
    fprintf(file, "--------------------------------\n");
    fclose(file);
}

/* check if file is empty: returns 1 for an empty file, 0 elsewise */
int file_is_empty(char *filename)
{
    struct stat s;
    int ret = 0;

    if (stat(filename, &s) == -1) {
	printf("Error: unable to get stats for file %s: %s\n",
		filename, strerror(errno));
	return 0;
    }

    if (s.st_size == 0)
	ret = 1;

    dbg("%s is%sempty (st_size = %ld)", filename, (ret ? " " : " not "), s.st_size);
    return ret;
}
