/* command.c: implementation of command functions */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "file.h"
#include "macro.h"
#include "command.h"
#include "configfile.h"
#include "utils.h"
#include "git.h"

/* private functions */
static void create_note(char *file);
static void list(char *file);
static void edit_note(char *file);
static void rm_note(char *file);
static void append_to_note(char *file);
static void show_note(char *file);
static void dump_note(char *file);
static void grep_notes(char *unused);
static void usage(void);

char *progname = "notes";   /* initialize to a default value */

static void usage(void)
{
    printf(
"Usage: %s <command> <args>\n"
"Valid commands\n"
"    - create <note>            Create a new note file\n"
"    - ls [subdir]              List all notes\n"
"    - edit <note>              Edit an existing note\n"
"    - rm <note>                Remove a note or subdir\n"
"    - show <note>              Display a note using NOTES_VIEWER\n"
"    - dump <note>              Dump a note on STDOUT\n"
"    - append <note>            Append stdin to a note\n"
"    - grep [opts] <pattern>    Find a pattern in the notes\n"
"    - help                     Display this help\n",
    progname);
    exit(1);
}

void parse_commandline(int argc, char *argv[])
{
    char *arg = NULL;
    char *com = NULL;
    
    progname = *argv++;
    argc--; /* decrease argc so it represents the number of remaining cmdline
	     * arguments to parse */

    if (argc == 0)
	usage();

    /* commands which can take no argument */
    com = *argv++;
    argc--;
    arg = *argv;
    if (strcmp(com, "help") == 0) {
	if (argc)
	    printf("Too many arguments\n");
	usage();
    } else if (strcmp(com, "ls") == 0) {
	if (argc == 0) {
	    arg = NULL;
	} else if (argc > 1) {
	    printf("Too many arguments for command \"%s\"\n", com);
	    usage();
	}
	list(arg);
    } else if (strcmp(com, "create") == 0) {
	if (argc != 1) {
	    printf("Too many arguments for command \"%s\"\n", com);
	    usage();
	}
	create_note(arg);
    } else if (strcmp(com, "edit") == 0) {
	if (argc != 1) {
	    printf("Too many arguments for command \"%s\"\n", com);
	    usage();
	}
	edit_note(arg);
    } else if (strcmp(com, "rm") == 0) {
	if (argc != 1) {
	    printf("Too many arguments for command \"%s\"\n", com);
	    usage();
	}
	rm_note(arg);
    } else if (strcmp(com, "show") == 0) {
	if (argc != 1) {
	    printf("Too many arguments for command \"%s\"\n", com);
	    usage();
	}
	show_note(arg);
    } else if (strcmp(com, "dump") == 0) {
	if (argc != 1) {
	    printf("Too many arguments for command \"%s\"\n", com);
	    usage();
	}
	dump_note(arg);
    } else if (strcmp(com, "append") == 0) {
	if (argc != 1) {
	    printf("Too many arguments for command \"%s\"\n", com);
	    usage();
	}
	append_to_note(arg);
    } else if (strcmp(com, "grep") == 0) {
	if (argc == 0) {
	    printf("Command \"%s\" takes at least one argument\n", com);
	    usage();
	}
	/* we want to copy all the args to a malloc()'ed string and protect the
	 * last arg with quotes, so we add 2 for the quotes */
	int len = sum_strlen(argc, argv) + argc + 2;
#ifdef DEBUG
	printf("Received argv = { ");
	for (int i = 0 ; i < argc ; i++)
	    printf("%s (len %d), ", argv[i], strlen(argv[i]));
	printf("}, total lenght = %d\n", len);
#endif
	str_malloc(arg, len);
	/* check that all the command line args before the last are options
	 * i.e. they start with '-' */
	while (argc > 1) {
	    if (**argv == '-') {
		strcat(arg, *argv);
		strcat(arg, " ");
		argv++;
		argc--;
	    } else {
		error("Command %s doesn't take options after pattern", com);
	    }

	}
	/* we encapsulate the last argument in quotes */
	strcat(arg, "\"");
	strcat(arg, *argv);
	strcat(arg, "\"");
	dbg("Built string \"%s\"", arg);
	grep_notes(arg);
    } else {
	printf("Unrecognized command \"%s\"\n", com);
	usage();
    }
}

static void create_note(char *file)
{
    char *arg = build_full_path(file);

    if (file_exists(arg)) {
	printf("Warning: %s already exists, editing\n", file);
    } else {
	/* create file and path to file if necessary */
	create_file(file);
    }

    /* open the file for edition */
    edit_note(file);
}

static void list(char *file)
{
    /* use tree command to display the notes directory structure */
    char *fmt = "tree -CR --noreport %s";
    char *arg = conf->notesdir;

    if (file != NULL) {
	arg = build_full_path(file); /* cannot be NULL (see build_full_path) */

	/* check if file exists in the hierarchy and if it is a directory */
	if (!file_exists(arg)) {
	    free(arg);
	    error("File %s doesn't exist in %s\n", file, conf->notesdir);
	}
	if (!path_is_dir(arg)) {
	    free(arg);
	    error("%s is not a valid subdirectory path in %s\n", file, conf->notesdir);
	}
    }

    exec_cmd(fmt, arg);
    if (arg != conf->notesdir)
	free(arg);
}

static void edit_note(char *file)
{
    char *env_var = "EDITOR";
    char *command = getenv(env_var);

    char *opts = conf->ed_opts;

    if (command == NULL) {
	error("%s is not set\n", env_var);
    }

    char *arg = build_full_path(file);

    if (!file_exists(arg))
	error("%s doesn't exist. Use create command instead\n", file);

    if (path_is_dir(arg))
	error("%s is a directory. Choose another name\n", file);

    if (opts)
	exec_cmd("%s %s %s", command, opts, arg);
    else
	exec_cmd("%s %s", command, arg);
    free(arg);
#ifdef WITH_GIT
    if (conf->git)
	commit("Edition", file);
#endif
}

/* find a pattern within the notes in the tree */
static void grep_notes(char *arg)
{
    char *cmd = "grep -r";
    char *dir = conf->notesdir;

    /* pipe through sed to remove notesdir */
    exec_cmd("%s %s %s | sed -e 's@%s/@@'", cmd, arg, dir, dir);
}

/* removes a note or a subdirectory of NOTES_DIR */
static void rm_note(char *file)
{
    char *arg = build_full_path(file);

    /* use rm -rf command as remove() doesn't delete non empty dirs */
    char *command = "rm -rf";

    /* check if first character is '/' */
    if (*file == '/')
	error("Name cannot start with '/'\n");

    /* check if file exists in the hierarchy */
    if (!file_exists(arg)) {
	free(arg);
	error("File %s doesn't exist in %s\n", file, conf->notesdir);
    }
    /* make sure that we want to remove a directory and everything inside */
    if (path_is_dir(arg)) {
	printf(
	    "I will delete %s and all notes and subdirectories it contains.\n",
	    file);
	printf("Are you sure ([y]/n)? ");
	int answer = getchar();
	if (!(answer == '\n' || answer == 'y' || answer == 'o')) {
	    printf("Aborting\n");
	    return;
	}
    }
    printf("Deleting %s...\n", file);
    exec_cmd("%s %s", command, arg);
    if (arg != conf->notesdir)
	free(arg);
#ifdef WITH_GIT
    if (conf->git)
	commit_all("Deletion");
#endif
}

static void append_to_note(char *file)
{
    char *arg = build_full_path(file);

    /* check if file exists in the hierarchy */
    if (!file_exists(arg)) {
	printf("Warning: File %s doesn't exist, creating it\n", file);
	create_file(file); /* create if it doesn't exist */
    } else if (path_is_dir(arg)) { /* check that the file is not a dir */
	free(arg);
	error("File %s is a directory\n", file);
    }

    FILE *f = fopen(arg, "a");
    if (f == NULL) {
	free(arg);
	error("Unable to open %s in append mode: %s\n", file, strerror(errno));
    }

    free(arg);
    int input;
    /* copy all input to file. TODO reimplement with read/write */
    while ((input = fgetc(stdin)) != EOF) {
	if (fputc((char) input, f) == EOF) {
	    error("Unable to copy character from stdin to %s: %s\n", file, 
		  strerror(errno));
	}
    }
#ifdef WITH_GIT
    if (conf->git)
	commit("Edition", file);
#endif
}

static void dump_note(char *file)
{
    char *arg = build_full_path(file);

    /* check if file exists in the hierarchy */
    if (!file_exists(arg)) {
	free(arg);
	error("File %s doesn't exist in %s\n", file, conf->notesdir);
    }
    /* check that file is not a directory */
    if (path_is_dir(arg)) {
	free(arg);
	error("File %s is a directory\n", file);
    }

    FILE *f = fopen(arg, "r");
    if (f == NULL) {
	free(arg);
	error("unable to open %s in read mode: %s\n", file, strerror(errno));
    }

    free(arg);
    int content;
    /* dump all the content of f to stdout */
    while ((content = fgetc(f)) != EOF) {
	if (fputc((char) content, stdout) == EOF) {
	    error("Unable to output character to stdout: %s\n", strerror(errno));
	}
    }
}

static void shift(char *s, int i)
{
    char *r = s+i;
    while (*s != '\0') {
	*s = *r;
	r++;
	s++;
    }
}

char **build_argv(char *command, char *file) 
{
#ifdef DEBUG
    printf("%s: initial command = \"%s\"\n", __func__, command);
#endif
    int fix = 3;
    int opt_count = 0;
    char *p = command;
    int whitecount = 0;
    /* strip beginning spaces */
    while (*p++ == ' ') {
	whitecount++;
    }
    shift(command, whitecount);
#ifdef DEBUG
    printf("%s: shift(p, %d) = \"%s\"\n", __func__, whitecount, command);
#endif
    p = command;
    /* remove consecutive spaces assuming the string is not empty */
    while (*(p+1) != '\0') {
	if (*p == ' ' && *(p+1) == ' ') {
	    shift(p+1, 1);
	    p--;
	}
	p++;
    }
    p = command;
    do {
	/* consider the case of long options */
	if ((*p == '-' || *p == ' ') && *(p+1) != '-')
	    opt_count++;
    } while (*++p);
#ifdef DEBUG
    printf("%s: Found %d options in command (\"%s\")\n",
	    __func__, opt_count, command);
#endif
    /* create a malloc()'ed array of pointer to char */
    char **argv = malloc((opt_count+fix)*sizeof(char *));
    char *optbuf = malloc((strlen(command)+1)*sizeof(char));
    strcpy(optbuf, command);
    if (argv == NULL)
        error("Unable to allocate memory for **argv: %s\n", strerror(errno));

    int i = 0;
    int dash_count = 0;
    while ((p = strsep(&optbuf, "- ")) != NULL) {
#ifdef DEBUG
	printf("%s: p = \"%s\"\n", __func__, p);
#endif
	if (strlen(p) == 0) {
	    dash_count++;
	    continue; /* skip empty strings */
	}
	char *q = malloc((strlen(p)+dash_count+1)*sizeof(char)); /* +2 as we add one
								  * character */
	if (q == NULL)
	    printf("Unable to allocate memory for option string\n");
	memset(q, '-', dash_count);
	q[dash_count] = '\0';
	dash_count = 0;
	strcat(q, p);
	/* remove trailing whitespaces */
	char *qend = index(q, '\0'); 
	for (char *r = --qend ; *r == ' ' ; r--)
	    *r = '\0';
	argv[i++] = q;
    }
    argv[i++] = file;
    argv[i] = NULL;
#ifdef DEBUG
    char **print = argv;
    printf("%s: argv = { ", __func__);
    do {
	printf("\"%s\" ", *print);
    } while (*++print != NULL);
    printf(" }\n");
#endif
    free(optbuf);
    return argv;
}

static void show_note(char *file)
{
    char *arg = build_full_path(file);
    char *command = conf->viewer;

    /* check if file exists in the hierarchy */
    if (!file_exists(arg)) {
	free(arg);
	error("File %s doesn't exist in %s\n", file, conf->notesdir);
    }
    /* check that file is not a directory */
    if (path_is_dir(arg)) {
	free(arg);
	error("File %s is a directory\n", file);
    }
    
    if (command == NULL)
	command = "cat";
    
    exec_cmd("%s %s", command, arg);
    free(arg);
}
