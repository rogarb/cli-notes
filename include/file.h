/******************************************************************************
 * file.h: File management infrastructure
 *****************************************************************************/
#ifndef FILE_H
#define FILE_H

/* helper function to recursively create a file and the necessary dirs */
void create_file(char *filename);

/* test functions */
int file_exists(char *filename);
int file_is_empty(char *filename);
int path_is_dir(char *path);

/* builds a malloc()'ed string containing the full path to filename */
/* exits on error */
char *build_full_path(char *filename);

#endif /* End of FILE_H */
