/******************************************************************************
 * configfile.h: Config file related functions
 *****************************************************************************/
#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include "macro.h"

/* global settings */
#define NOTESDIR_ENV	"NOTES_DIR"
#define EDITOROPTS_ENV	"NOTES_ED_OPTS"
#define VIEWER_ENV	"NOTES_VIEWER"
#define HOME_ENV	"HOME"
#define CONFIG_DIR	".config"
#define CONFIG_FILE	"notes-cli"

/* valid keys in configfile */
#define CONF_GIT_KEY	    "with_git"
#define CONF_NOTESDIR_KEY   "notesdir"
#define CONF_EDITOROPTS_KEY "editor_options"
#define CONF_VIEWER_KEY	    "viewer"

/* configuration assertion macros */
#define assert_config() \
    do {\
	if (conf == NULL)\
	    error("BUG: config has not been set up");\
    } while (0)

typedef struct config {
    char *notesdir;	/* full path to the notes directory */
    char *ed_opts;	/* options passed to $EDITOR */
    char *viewer;	/* viewer program for cat command */
#ifdef WITH_GIT
    int git;		/* if set, use git for tracking changes */
#endif /* WITH_GIT */
} config_t;

extern config_t *conf;

void setup(void);
void quit(void);

#endif /* End of CONFIGFILE_H */
