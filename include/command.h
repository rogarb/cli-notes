/******************************************************************************
 * command.h: Command functions
 *****************************************************************************/
#ifndef COMMAND_H
#define COMMAND_H

void parse_commandline(int argc, char *argv[]);

#endif /* End of COMMAND_H */
