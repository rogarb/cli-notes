/******************************************************************************
 * git.h: Git versioning functions
 *****************************************************************************/
#ifndef GIT_H
#define GIT_H

#ifdef WITH_GIT

/* git function wrapper */
#define exec_git(func, errmsg, ...) \
    do {\
	int __git_err = func;\
	if (__git_err > 0) \
	    error(errmsg " (err %d)", ##__VA_ARGS__, __git_err);\
    } while (0)

void create_gitignore(char *basedir);
void exit_git(void);
void commit(const char *msg, char *file);
void commit_multi(const char *msg, int nfiles, ...);
void commit_all(char *msg);
void setup_git(void);
#else
void __nop_git(void); /* does nothing */
#endif /* WITH_GIT */

#endif /* End of GIT_H */
