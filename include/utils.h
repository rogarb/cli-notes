/******************************************************************************
 * utils.h: Various utility functions
 *****************************************************************************/
#ifndef UTILS_H
#define UTILS_H
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "macro.h"

/* allocate memory in ptr which is suitable to hold a string of length len
 * memory is set to zero */
#define str_malloc(ptr, len) \
    do {\
	if ((ptr = malloc((len+1)*sizeof(char))) == NULL) \
	    error("unable to allocate %d bytes of memory: %s",\
		    len+1, strerror(errno));\
	memset(ptr, 0, (len+1)*sizeof(char));\
    } while (0)

/* strlen equivalent */
int intlen(int i);
int sum_strlen(int count, char **array);

/* command execution functions */
void exec_cmd(const char *fmt, ...);

/* string test functions */
int is_number(char *string);

/* string manipulation */
void strip_spaces(char *string);
void strip_quotes(char *string);
char *expand(char *path);
char *get_basedir(char *path);
char *get_basename(char *path);
char *build_str(char *fmt, ...);

/* array related */
int has_item(int item, int *list, int count);

#endif /* End of UTILS_H */
