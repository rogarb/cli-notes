#!/bin/sh

echo -n "#compdef " 
awk  -F '=' '/^EXE/ {print $2}' Makefile
echo -n "_arguments '"
awk -F '"' 'BEGIN {printf "1:command:("} /^command_t/{flag=1; next} /^};/ {flag=0} flag{printf "%s ",$2} END {printf ")"}' src/command.c
echo "' '2:arg:_files -W \$NOTES_DIR'"
